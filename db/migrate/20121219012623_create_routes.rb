class CreateRoutes < ActiveRecord::Migration
  def change
    create_table :routes do |t|
      t.string :number
      t.string :name
      t.string :color
      t.references :shape
      t.references :service_type

      t.timestamps
    end
    add_index :routes, :shape_id
    add_index :routes, :service_type_id
  end
end
