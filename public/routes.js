var uta_wrapper = (function() {
	var Route = Backbone.Model.extend({
	});

	var RouteCollection = Backbone.Collection.extend({
		"model" : Route,
		"url": "/service_types/1/routes"
	});

	var RouteSelect = Backbone.View.extend({
		"tagName" : "select",
		"option_template" : _.template('<option value="<%= number %>"><%= number %> <%= name %></option>'),
		"render" : function() {
			this.$el.html('');
			this.$el.append('<option value=""></option>');
			this.model.each(function(route) {
				var option_element = this.option_template( {name: route.get('name'), number: route.get('number')});
				this.$el.append(option_element);
			}, this);
			this.model.on('reset', this.render, this);
		}
	});

	var AppendRouteSelectorToBody = function(service_type_id) {
		route_collection_url = '/service_types/' + service_type_id + '/routes.json';
		routes = new RouteCollection();
		routes.fetch({"url" : route_collection_url});

		var select = new RouteSelect({ "model" : routes });
		select.render();
		$('body').append(select.$el);
	};

	return {
		"RouteCollection" : RouteCollection,
		"RouteSelect" : RouteSelect,
		"Route" : Route,
		"AppendRouteSelectorToBody" : AppendRouteSelectorToBody
	}
})();

