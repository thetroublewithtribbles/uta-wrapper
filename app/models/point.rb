class Point < ActiveRecord::Base
  belongs_to :shape
  attr_accessible :lat, :lon, :sequence
end
