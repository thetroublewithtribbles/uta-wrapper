class ServiceTypesController < ApplicationController
  # GET /service_types
  # GET /service_types.json
  def index
    @service_types = ServiceType.all

    render json: @service_types
  end

  # GET /service_types/1
  # GET /service_types/1.json
  def show
    @service_type = ServiceType.find(params[:id])

    render json: @service_type
  end

  # GET /service_types/new
  # GET /service_types/new.json
  def new
    @service_type = ServiceType.new

    render json: @service_type
  end

  # POST /service_types
  # POST /service_types.json
  def create
    @service_type = ServiceType.new(params[:service_type])

    if @service_type.save
      render json: @service_type, status: :created, location: @service_type
    else
      render json: @service_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /service_types/1
  # PATCH/PUT /service_types/1.json
  def update
    @service_type = ServiceType.find(params[:id])

    if @service_type.update_attributes(params[:service_type])
      head :no_content
    else
      render json: @service_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /service_types/1
  # DELETE /service_types/1.json
  def destroy
    @service_type = ServiceType.find(params[:id])
    @service_type.destroy

    head :no_content
  end
end
