class AddSequenceOrderToPoint < ActiveRecord::Migration
  def up
    add_column :points, :sequence, :integer
  end

  def down
    remove_column :points, :sequence
  end
end
