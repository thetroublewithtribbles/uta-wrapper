class Shape < ActiveRecord::Base
  belongs_to :route
  has_many :points
  # attr_accessible :title, :body
end
