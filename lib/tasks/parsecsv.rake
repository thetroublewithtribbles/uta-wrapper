require 'csv'
require 'pp'

class Importer
  def initialize
    @utadata_path = '/vagrant/uta-data'
  end

  def statically_define_service_types
    trax = ServiceType.new
    bus = ServiceType.new
    fr = ServiceType.new
    ski = ServiceType.new

    trax.name = 'TRAX'
    bus.name = 'Bus'
    fr.name = 'FrontRunner'
    ski.name = 'Ski'

    trax.id = 1
    bus.id = 2
    fr.id = 3
    ski.id = 4

    ski.save
    trax.save
    bus.save
    fr.save
  end

  def parse_routes
    arr = CSV.read("#{@utadata_path}/routes.txt")
    arr[1..-1].each do |row|
      route_to_add = Route.new
      route_to_add.id = row[5]
      route_to_add.name = row[0]
      route_to_add.number = row[8]
      route_to_add.color = '#' + row[3]
      set_route_service_type!(route_to_add)
      route_to_add.save
    end
  end

  def set_route_service_type!(route)
    route.service_type_id = 2
    /ski/i.match(route.name) {|m| route.service_type_id = 4 }
    /trax/i.match(route.name) {|m| route.service_type_id = 1 }
    /frontrunner/i.match(route.name) {|m| route.service_type_id = 3 }
  end

  def parse_points
    arr = CSV.read("#{@utadata_path}/shapes.txt")
    existing_shape_ids = {}
    arr[1..-1].each do |row|
      begin
        p = Point.new
        p.shape_id = row[0]
        p.lat = row[1]
        p.lon = row[2]
        p.sequence = row[3]
        p.save
  
        unless existing_shape_ids[p.shape_id]
  	  s = Shape.new
  	  s.id = p.shape_id
  	  s.save
  	  existing_shape_ids[p.shape_id] = true
	end
      rescue Exception => e
        puts "Error importing point/shape: " + e.message
        pp s
      end
    end
  end

  def parse_stops
    arr = CSV.read("#{@utadata_path}/stops.txt")
    arr[1..-1].each do |row|
      begin
        s = Stop.new
        s.lat = row[0]
        s.lon = row[2]
        s.id = row[3]
        s.name = row[7]
        s.save
      rescue Exception => e
        puts "Error importing stop: " + e.message
        pp s
      end
    end
  end

  def connect_routes_and_shapes
    arr = CSV.read("#{@utadata_path}/trips.txt")
    arr[1..-1].each do |row|
      begin
        s = Shape.find(row[4])
        s.route_id = row[1]
        s.save
      rescue Exception => e
        puts "Error connecting shape and route: " + e.message
        pp s
      end
    end
  end

  def drop_all
    Stop.delete_all
    Shape.delete_all
    Route.delete_all
    Point.delete_all
    ServiceType.delete_all
  end
end


namespace :import do 
  desc "Import routes from uta"
  task :routes => :environment do
    importer = Importer.new
    importer.parse_routes
  end

  task :service_types => :environment do
    importer = Importer.new
    importer.statically_define_service_types
  end

  task :points => :environment do
    importer = Importer.new
    importer.parse_points
  end

  task :stops => :environment do
    importer = Importer.new
    importer.parse_stops
  end

  task :connect_routes_and_shapes => :environment do
    importer = Importer.new
    importer.connect_routes_and_shapes
  end

  task :all => :environment do
    importer = Importer.new
    importer.statically_define_service_types
    importer.parse_routes
    importer.parse_points
    importer.parse_stops
    importer.connect_routes_and_shapes
  end

  task :drop => :environment do
    importer = Importer.new
    importer.drop_all
  end
end
