class VehiclesController < ApplicationController
  def index
    adapter = RealTimeAPIAdapter.new
    vehicles = adapter.get_vehicles(703)
    render json: vehicles
  end
end
