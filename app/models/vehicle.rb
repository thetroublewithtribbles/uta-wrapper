require 'json'

class Vehicle
  attr_accessor :lat, :lon, :direction, :last_update

  def as_json(options = {})
    {
      lat: @lat,
      lon: @lon,
      direction: @direction,
      last_update: @last_update
    }
  end
end
