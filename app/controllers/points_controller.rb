class PointsController < ApplicationController
  # GET /points
  # GET /points.json
  def index
    @points = Point.find_all_by_shape_id(params[:shape_id])

    render json: @points
  end

  # GET /points/1
  # GET /points/1.json
  def show
    @point = Point.find(params[:id])

    render json: @point
  end

  # GET /points/new
  # GET /points/new.json
  def new
    @point = Point.new

    render json: @point
  end

  # POST /points
  # POST /points.json
  def create
    @point = Point.new(params[:point])

    if @point.save
      render json: @point, status: :created, location: @point
    else
      render json: @point.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /points/1
  # PATCH/PUT /points/1.json
  def update
    @point = Point.find(params[:id])

    if @point.update_attributes(params[:point])
      head :no_content
    else
      render json: @point.errors, status: :unprocessable_entity
    end
  end

  # DELETE /points/1
  # DELETE /points/1.json
  def destroy
    @point = Point.find(params[:id])
    @point.destroy

    head :no_content
  end
end
