class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.float :lat
      t.float :lon
      t.references :shape

      t.timestamps
    end
    add_index :points, :shape_id
  end
end
