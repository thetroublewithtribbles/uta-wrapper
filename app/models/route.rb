class Route < ActiveRecord::Base
  has_one :shape
  belongs_to :service_type
  attr_accessible :color, :name, :number
end
