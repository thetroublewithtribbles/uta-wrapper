# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121219021327) do

  create_table "points", :force => true do |t|
    t.float    "lat"
    t.float    "lon"
    t.integer  "shape_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "sequence"
  end

  add_index "points", ["shape_id"], :name => "index_points_on_shape_id"

  create_table "routes", :force => true do |t|
    t.string   "number"
    t.string   "name"
    t.string   "color"
    t.integer  "shape_id"
    t.integer  "service_type_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "routes", ["service_type_id"], :name => "index_routes_on_service_type_id"
  add_index "routes", ["shape_id"], :name => "index_routes_on_shape_id"

  create_table "service_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "shapes", :force => true do |t|
    t.integer  "route_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "shapes", ["route_id"], :name => "index_shapes_on_route_id"

  create_table "stops", :force => true do |t|
    t.string   "name"
    t.float    "lat"
    t.float    "lon"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
