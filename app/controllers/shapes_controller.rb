class ShapesController < ApplicationController
  # GET /shapes
  # GET /shapes.json
  def index
		route = Route.find_by_number(params[:route_id])
    @shapes = Shape.find_all_by_route_id(route.id)

    render json: @shapes
  end

  # GET /shapes/1
  # GET /shapes/1.json
  def show
    @shape = Shape.find(params[:id])

    render json: @shape
  end

  # GET /shapes/new
  # GET /shapes/new.json
  def new
    @shape = Shape.new

    render json: @shape
  end

  # POST /shapes
  # POST /shapes.json
  def create
    @shape = Shape.new(params[:shape])

    if @shape.save
      render json: @shape, status: :created, location: @shape
    else
      render json: @shape.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /shapes/1
  # PATCH/PUT /shapes/1.json
  def update
    @shape = Shape.find(params[:id])

    if @shape.update_attributes(params[:shape])
      head :no_content
    else
      render json: @shape.errors, status: :unprocessable_entity
    end
  end

  # DELETE /shapes/1
  # DELETE /shapes/1.json
  def destroy
    @shape = Shape.find(params[:id])
    @shape.destroy

    head :no_content
  end
end
