class RoutesController < ApplicationController
  # GET /routes
  # GET /routes.json
  def index
    @routes = Route.find_all_by_service_type_id(params[:service_type_id])
    render json: @routes
  end

  # GET /routes/1
  # GET /routes/1.json
  def show
    @route = Route.find_by_number(params[:id])
    render json: @route
  end

  # GET /routes/new
  # GET /routes/new.json
  def new
    @route = Route.new
    render json: @route
  end

  # POST /routes
  # POST /routes.json
  def create
    @route = Route.new(params[:route])

    if @route.save
      render json: @route, status: :created, location: @route
    else
      render json: @route.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /routes/1
  # PATCH/PUT /routes/1.json
  def update
    @route = Route.find(params[:id])

    if @route.update_attributes(params[:route])
      head :no_content
    else
      render json: @route.errors, status: :unprocessable_entity
    end
  end

  # DELETE /routes/1
  # DELETE /routes/1.json
  def destroy
    @route = Route.find(params[:id])
    @route.destroy

    head :no_content
  end
end
