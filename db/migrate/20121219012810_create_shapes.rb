class CreateShapes < ActiveRecord::Migration
  def change
    create_table :shapes do |t|
      t.references :route

      t.timestamps
    end
    add_index :shapes, :route_id
  end
end
