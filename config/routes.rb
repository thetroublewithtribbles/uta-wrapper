UTAWrapper::Application.routes.draw do

  resources :service_types do
    resources :routes do
      resources :shapes do
      	resources :points, except: :edit
			end
      resources :stops, except: :edit
      resources :vehicles, only: :index
    end
  end

end
